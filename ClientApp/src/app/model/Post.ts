import { Comment } from './Comment';
export interface Post {
  id: number;
  username: string;
  title: string;
  image: string;
  genre: string;
  date: Date;
  summary: string;
  content: string;
  comments: Comment[];
}
