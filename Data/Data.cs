﻿using Microsoft.AspNetCore.Http;
using MyBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog.Data
{
    public static class Data
    {
        public static List<PostModel> PostList { get; set; }
        public static List<PostModel> Init()
        {
            PostList = new List<PostModel>();
            CommentModel comment = new CommentModel("LuanTN","LuanNT@gmail.com", System.DateTime.Now ,"Bai viet hay qua");
            PostModel post = new PostModel("KhoaNN", "This is Blog test", "m-farmerboy.jpg","TesT Genre", System.DateTime.Now, "This is Summary", "This Content");
            post.Comments.Add(comment); 
            PostList.Add(post);

            comment = new CommentModel("LuanTN","LuanNT@gmail.com", System.DateTime.Now ,"Bai viet hay qua");
            post = new PostModel("KhoaNN", "This is Blog test", "m-farmerboy.jpg","TesT Genre", System.DateTime.Now, "This is Summary", "This Content");
            post.Comments.Add(comment);
            PostList.Add(post);

            comment = new CommentModel("LuanTN","LuanNT@gmail.com", System.DateTime.Now ,"Bai viet hay qua");
            post = new PostModel("KhoaNN", "This is Blog test", "m-farmerboy.jpg","TesT Genre", System.DateTime.Now, "This is Summary", "This Content");
            post.Comments.Add(comment);
            PostList.Add(post);

            comment = new CommentModel("LuanTN","LuanNT@gmail.com", System.DateTime.Now ,"Bai viet hay qua");
            post.Comments.Add(comment);
            post = new PostModel("KhoaNN", "This is Blog test", "m-farmerboy.jpg","TesT Genre", System.DateTime.Now, "This is Summary", "This Content");
            PostList.Add(post);

            comment = new CommentModel("LuanTN","LuanNT@gmail.com", System.DateTime.Now ,"Bai viet hay qua");
            post = new PostModel("KhoaNN", "This is Blog test", "m-farmerboy.jpg","TesT Genre", System.DateTime.Now, "This is Summary", "This Content");
            post.Comments.Add(comment);
            PostList.Add(post);
            comment = new CommentModel("LuanTN", "LuanNT@gmail.com", System.DateTime.Now, "Bai viet hay qua");
            post = new PostModel("Michael Shpilt ", "Is C# Slower Than C++?", "csharp-performance-vs-c.jpg", ".Net", System.DateTime.Now, "Is C# slower than C++? That’s a pretty big question. As a junior developer, I was sure that the answer is “Yes, definitely”. Now that I’m more experienced, I know that this question is not obvious and even quite complicated.", "This Content");
            post.Comments.Add(comment);
            PostList.Add(post);

            comment = new CommentModel("LuanTN", "LuanNT@gmail.com", System.DateTime.Now, "Bai viet hay qua");
            post = new PostModel("Michael Shpilt ", "10 Essential Debugging Tools for C# .NET Development", "debugging-tools-csharp-dotnet.jpg", ".Net", System.DateTime.Now, "When it comes to debugging software, tools are extremely important. Get the right tool and you extract the right information. Get the right information and you can find the root cause of the issue. Find the root cause and you’ve solved the bug.", "This Content");
            post.Comments.Add(comment);
            PostList.Add(post);
            return PostList;
        }
    }
}
